var express = require('express'),
  app = express(),
  port = process.env.PORT || 3000;

var path = require('path');
app.use(express.static(__dirname + '/build/V1'));

app.listen(port);

console.log('Nyobama funcionando desde docker: ' + port);

app.get("/", function(req, res)
{
  res.sendFile("index.html", {root:"."});
})
